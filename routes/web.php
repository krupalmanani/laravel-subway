<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/meal-order/{meal_id}', 'OrderController@publicOrder');

Route::group(['middleware' => 'checkRole:admin'], function () {    
    Route::resource('user', 'UserController');
    Route::get('user/{user_id}/edit', 'UserController@edit')->name('user.edit');
    Route::post('meal/status', 'MealController@status')->name('meal.status');
    Route::resource('meal', 'MealController');
    Route::get('meal/{meal_id}/edit', 'MealController@edit')->name('meal.edit');
    Route::get('/order/{meal_id}', ['as' => 'order', 'uses' => 'OrderController@index'] );
    Route::post('status', 'OrderController@status' )->name('order.status');
});

Route::group(['middleware' => 'checkRole:user'], function () {
  Route::get('/shop/{meal_id}', ['as' => 'shop', 'uses' => 'ShopController@create'] );
  Route::get('order', 'OrderController@myOrder' )->name('order.index');
  Route::get('order/{order_id}/edit', 'OrderController@edit' )->name('order.edit');
  Route::post('order/{order_id}', 'OrderController@update' )->name('order.update');
  Route::resource('shop', 'ShopController');
});