@extends('header')

@section('content')
<div class="row">
 <div class="col-md-12">
  <br />
  <h3 aling="center">Update Order</h3>
  <br />
  @if(count($errors) > 0)
  <div class="alert alert-danger">
   <ul>
   @foreach($errors->all() as $error)
    <li>{{$error}}</li>
   @endforeach
   </ul>
  </div>
  @endif
  @if(\Session::has('success'))
  <div class="alert alert-success">
   <p>{{ \Session::get('success') }}</p>
  </div>
  @endif

  <form method="post" action="{{route('order.update',$order->id)}}">
   {{csrf_field()}}

   <input type="hidden" name="meal_id" value="{{$order->meal_id}}" />

   <div class="form-group">
    <label for="bread">What bread</label>
    <select name="bread" id="bread" class="form-control">
        @foreach($bread as $details)
            <option value="{{$details->id}}" {{($order->bread == $details->id) ? 'selected' : ''}} >{{$details->bread_title}}</option>
        @endforeach
    </select>
   </div>
   
   <div class="form-group">
    <label for="bread_size">Size of the bread</label>
    <select name="bread_size" id="bread_size" class="form-control">
        @foreach($bread_size as $details)
            <option value="{{$details->id}}" {{($order->bread_size == $details->id) ? 'selected' : ''}}>{{$details->bread_size}}</option>
        @endforeach
    </select>
   </div>

   <div class="form-group">
    <label for="sauce">Oven baked</label>
    <div class="form-check">
    <input class="form-check-input" type="radio" name="oven_baked" id="yes" value="Y" {{($order->oven_baked == 'Y') ? 'checked' : ''}} />
    <label class="form-check-label" for="yes">Yes</label>
    </div>
    <div class="form-check">
    <input class="form-check-input" type="radio" name="oven_baked" id="no" value="N" {{($order->oven_baked == 'N') ? 'checked' : ''}} />
    <label class="form-check-label" for="no">No</label>
    </div>
   </div>

   <div class="form-group">
    <label for="sandwich_taste">Taste of the sandwich</label>
    <select name="sandwich_taste" id="sandwich_taste" class="form-control">
        @foreach($sandwich_taste as $details)
            <option value="{{$details->id}}" {{($order->sandwich_taste == $details->id) ? 'selected' : ''}}>{{$details->taste}}</option>
        @endforeach
    </select>
   </div>

   <div class="form-group">
    <label for="extra">Extra’s</label>
        @foreach($extra as $key => $details)
        <div class="form-check">
            <input class="form-check-input" name="extra[]" type="checkbox" value="{{$details->id}}" id="check{{$key}}" {{in_array($details->id,explode(',',$order->extra)) ? "checked" : ""}} />
            <label class="form-check-label" for="check{{$key}}">{{$details->extra_item}}</label>
        </div>            
        @endforeach
   </div>

   <div class="form-group">
    <label for="sandwich_vegetables">What vegetables you want on the sandwich</label>
    <select name="sandwich_vegetables[]" id="sandwich_vegetables" class="form-control selectpicker" multiple>
        @foreach($sandwich_vegetables as $details)
            <option value="{{$details->id}}" {{(in_array($details->id, explode(',',$order->sandwich_vegetables))) ? 'selected' : ''}}>{{$details->vegetables_title}}</option>
        @endforeach
    </select>
   </div>

   <div class="form-group">
    <label for="sauce">What sauce</label>
    <select name="sauce[]" id="sauce" class="form-control selectpicker" multiple>
        @foreach($sauce as $details)
            <option value="{{$details->id}}" {{(in_array($details->id, explode(',',$order->sauce))) ? 'selected' : ''}}>{{$details->sauce_title}}</option>
        @endforeach
    </select>
   </div>
   <div class="form-group">
    <input type="submit" class="btn btn-primary" />
   </div>
  </form>
 </div>
</div>
@endsection

