@extends('header')

@section('content')

<div class="row">
 <div class="col-md-12">
   <br />
  <h3>Order Details</h3>
  <br />
  @if(\Session::has('success'))
  <div class="alert alert-success">
   <p>{{ \Session::get('success') }}</p>
  </div>
  @endif
  <table class="table table-bordered table-striped">
   <tr>
    <th>Name</th>
    <th>Email</th>
    <th>Meal</th>
    <th>Bread</th>
    <th>Size</th>
    <th>Sandwich taste</th>
    <th>Extra</th>
    <th>Sandwich vegetables</th>
    <th>Sauce</th>
    <th>Status</th>
   </tr>
     @if(count($orders))
          @foreach($orders as $row)
               <tr class={{ $row->status == "open" ? 'open' : 'not-open' }}>
                    <td>{{$row->name}}</td>
                    <td>{{$row->email}}</td>
                    <td>{{$row->meal_title}}</td>
                    <td>{{$row->bread_title}}</td>
                    <td>{{$row->bread_size}}</td>
                    <td>{{$row->taste}}</td>
                    <td>@if($row->extra) 
                         @foreach(explode(',',$row->extra) as $index)
                           {{$extras[$index]->extra_item}} <br/>
                         @endforeach
                    @endif</td>
                    <td>@if($row->sandwich_vegetables) 
                         @foreach(explode(',',$row->sandwich_vegetables) as $index)
                           {{$vegetables[$index]->vegetables_title}}<br/>
                         @endforeach
                    @endif</td>
                    <td>@if($row->sauce) 
                         @foreach(explode(',',$row->sauce) as $index)
                           {{$sauce[$index]->sauce_title}}<br/>
                         @endforeach
                    @endif</td>
                    <td>{{ucfirst($row->status)}}</td>
               </tr>
          @endforeach
     @else 
          <tr><td colspan="9"> No order found. </td></tr>
     @endif
  </table>
 </div>
</div>
<script type="text/javascript">
    let elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function(html) {
        let switchery = new Switchery(html,  { size: 'small' });
    });

$(document).ready(function(){

    $('.js-switch').change(function () {
          if(!$(this).is(':checked')){
               if(confirm('Are you sure you want to close order?')){    
                    $(this).removeAttr('checked').attr('disabled',true);
                    $(this).parent().parent().addClass('not-open').removeClass('open');
               }
          }else if(confirm('Are you sure you want to open order?')){
               $(this).attr("checked", "checked");
               $(this).parent().parent().addClass('open').removeClass('not-open');
          }

        let status = $(this).prop('checked') === true ? "open" : "close";
        let orderId = $(this).data('id');
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '{{ route('order.status') }}',
            data: {"_token": "{{ csrf_token() }}", 'status': status, 'orderId': orderId},
            success: function (data) {
                console.log(data.message);
            }
        });
        
        return false;
    });
});
</script> 
@endsection