@extends('header')

@section('content')

<div class="row">
 <div class="col-md-12">
   <br />
  <h3>Order Details</h3>
  <br />
  @if(\Session::has('success'))
  <div class="alert alert-success">
   <p>{{ \Session::get('success') }}</p>
  </div>
  @endif
  <table class="table table-bordered table-striped">
   <tr>
    <th>Name</th>
    <th>Email</th>
    <th>Meal</th>
    <th>Bread</th>
    <th>Size</th>
    <th>Sandwich taste</th>
    <th>Extra</th>
    <th>Sandwich vegetables</th>
    <th>Sauce</th>
    <th></th>
   </tr>
   @if(count($orders))
        @foreach($orders as $row)
            <tr class={{ $row->status == "open" ? 'open' : 'not-open' }}>
                <td>{{$row->name}}</td>
                <td>{{$row->email}}</td>
                <td>{{$row->meal_title}}</td>
                <td>{{$row->bread_title}}</td>
                <td>{{$row->bread_size}}</td>
                <td>{{$row->taste}}</td>
                <td>@if($row->extra) 
                        @foreach(explode(',',$row->extra) as $index)
                        {{$extras[$index]->extra_item}} <br/>
                        @endforeach
                @endif</td>
                <td>@if($row->sandwich_vegetables) 
                        @foreach(explode(',',$row->sandwich_vegetables) as $index)
                        {{$vegetables[$index]->vegetables_title}}<br/>
                        @endforeach
                @endif</td>
                <td>@if($row->sauce) 
                        @foreach(explode(',',$row->sauce) as $index)
                        {{$sauce[$index]->sauce_title}}<br/>
                        @endforeach
                @endif</td>
                <td>
                    @if($row->status == 'open')
                        <a href="{{route('order.edit', ['order_id' => $row->id])}}">Edit</a>
                    @else 
                        Closed
                    @endif
                </td>
            </tr>
        @endforeach
    @else 
          <tr><td colspan="9"> No order found. </td></tr>
     @endif   
  </table>
 </div>
</div>
@endsection