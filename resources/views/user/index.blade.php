@extends('header')

@section('content')

<div class="row">
 <div class="col-md-12">
 <br />
  <h3>User Details</h3>
  <br />
  @if(\Session::has('success'))
  <div class="alert alert-success">
   <p>{{ \Session::get('success') }}</p>
  </div>
  @endif
  <div align="right" class="pb-2">
   <a href="{{route('user.create')}}" class="btn btn-primary">Add User</a>
  </div>
  <table class="table table-bordered table-striped">
   <tr>
    <th>User Name</th>
    <th>Email Address</th>
    <th>Role</th>
    <th></th>
   </tr>
   @foreach($users as $row)
     <tr>    
        <td>{{$row->name}}</td>
        <td>{{$row->email}}</td>
        <td>{{ucfirst($row->role)}}</td>
        <td><a href="{{route('user.edit', ['user_id' => $row->id])}}">Edit</a></td>
     </tr>
   @endforeach
  </table>
 </div>
</div>

@endsection