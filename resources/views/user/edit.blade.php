@extends('header')

@section('content')

<div class="row">
 <div class="col-md-12">
  <br />
  <h3 aling="center">Edit User</h3>
  <br />
  @if(count($errors) > 0)
  <div class="alert alert-danger">
   <ul>
   @foreach($errors->all() as $error)
    <li>{{$error}}</li>
   @endforeach
   </ul>
  </div>
  @endif
  @if(\Session::has('success'))
  <div class="alert alert-success">
   <p>{{ \Session::get('success') }}</p>
  </div>
  @endif

  <form method="post" action="{{route('user.update',$user->id)}}">
   {{csrf_field()}}
   {{ method_field('PUT') }}

    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" id="name" class="form-control" placeholder="Enter Name" value="{{(old('name')) ? old('name') : $user->name}}" />
    </div>
   
    <div class="form-group">
        <label for="email">Email Address</label>
        <input type="email" name="email" id="email" class="form-control" placeholder="Enter Email" value="{{(old('email')) ? old('email') : $user->email}}" />
    </div>
    
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" id="password" class="form-control" placeholder="*****" />
    </div>

    <div class="form-group">
        <label for="password_confirmation">Confirm Password</label>
        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="*****" />
    </div>

   <div class="form-group">
    <label for="role">Role</label>
    <select name="role" id="role" class="form-control">
        <option value="">Select Role</option>
        <option value="admin" {{($user->role == 'admin') ? 'selected' : ''}} >Admin</option>
        <option value="user" {{($user->role == 'user') ? 'selected' : ''}}>User</option>
    </select>
   </div>

   <div class="form-group">
    <input type="submit" class="btn btn-primary" />
   </div>
  </form>
 </div>
</div>

@endsection