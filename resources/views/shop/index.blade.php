@extends('header')

@section('content')

<div class="row">
 <div class="col-md-12">
  <br />
  @if(\Session::has('success'))
  <div class="alert alert-success">
   <p>{{ \Session::get('success') }}</p>
  </div>
  @endif
  <table class="table table-bordered table-striped">
   <tr>
    <th>Meal</th>
   </tr>
   @foreach($meal as $row)
   <tr class={{ $row['is_open'] == 1 ? 'open' : 'not-open' }}>
    @if ($row['is_open'] == 1)
        <td><a href="{{route('shop', ['meal_id' => $row['id']])}}">{{$row['meal_title']}}</a></td>
    @else
        <td>{{$row['meal_title']}}</td>
    @endif
   </tr>
   @endforeach
  </table>
 </div>
</div>
@endsection