@extends('header')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if (Auth::user())
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        Welcome {{ Auth::user()->name }} !
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection
