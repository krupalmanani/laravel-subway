@extends('header')

@section('content')

<div class="row">
 <div class="col-md-12">
  <br />
  <h3>Meal Details</h3>
  <br />
  @if($message = Session::get('success'))
  <div class="alert alert-success">
   <p>{{$message}}</p>
  </div>
  @endif
  <div align="right">
   <a href="{{route('meal.create')}}" class="btn btn-primary">Add</a>
   <br />
   <br />
  </div>
  <table class="table table-bordered table-striped">
   <tr>
    <th>Meal</th>
    <th>Is Open</th>
    <th>Date</th>
    <th></th>
    <th>View Orders</th>
   </tr>
   @foreach($meal as $row)
   <tr>
    <td>{{$row['meal_title']}}</td>
    <td><input type="checkbox" data-id="{{ $row['id'] }}" name="status" class="js-switch" {{ $row['is_open'] == 1 ? 'checked' : '' }}></td>
    <td>{{ date('d-m-Y', strtotime($row['meal_date']))}}</td>
    <td><a href="{{route('meal.edit', ['meal_id' => $row['id']])}}">Edit</a></td>
    <td><a href="{{route('order', ['meal_id' => $row['id']])}}">View</a></td>
   </tr>
   @endforeach
  </table>
 </div>
</div>
<script type="text/javascript">
    let elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function(html) {
        let switchery = new Switchery(html,  { size: 'small' });
    });

    $(document).ready(function(){
    $('.js-switch').change(function () {
        /* $('.js-switch').prop('checked',false).removeAttr('checked');
         */

       /*  $('.js-switch').attr('checked', false).trigger("click");
        if ($(this).prop('checked') === true){
            console.log("Ad");
          //  $(this).attr('checked', true).trigger("click");
        } */
         

        let status = $(this).prop('checked') === true ? 1 : 0;
        let mealId = $(this).data('id');
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '{{ route('meal.status') }}',
            data: {"_token": "{{ csrf_token() }}", 'status': status, 'mealId': mealId},
            success: function (data) {
                location.reload();
                console.log(data.message);
            }
        });
    });
});
</script> 
@endsection