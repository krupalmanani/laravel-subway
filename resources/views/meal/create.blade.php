@extends('header')



@section('content')
<div class="row">
 <div class="col-md-12">
  <br />
  <h3 aling="center">Add Meal</h3>
  <br />
  @if(count($errors) > 0)
  <div class="alert alert-danger">
   <ul>
   @foreach($errors->all() as $error)
    <li>{{$error}}</li>
   @endforeach
   </ul>
  </div>
  @endif
  @if(\Session::has('success'))
  <div class="alert alert-success">
   <p>{{ \Session::get('success') }}</p>
  </div>
  @endif

  <form method="post" action="{{url('meal')}}" autocomplete="off">
   {{csrf_field()}}
   <div class="form-group">
    <input type="text" name="meal_title" class="form-control" placeholder="Enter Meal" />
   </div>
   <div class="form-group">
    <input type="text" name="meal_date" class="date form-control" placeholder="Enter Date" />
   </div>
   <div class="form-group">
    <input type="checkbox"  name="is_open" class="js-switch" >
    </div>
   <div class="form-group">
    <input type="submit" class="btn btn-primary" />
   </div>
  </form>
 </div>
</div>
<script type="text/javascript">
    jQuery('.date').datepicker({  
       format: 'yyyy-mm-dd',
       autoclose: true,
       orientation: "auto",
     });  

     let elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function(html) {
        let switchery = new Switchery(html,  { size: 'small' });
    });
</script> 
@endsection

