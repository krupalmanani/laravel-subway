@if (Auth::user()->role == 'admin')
    <li class="nav-item">
        <a class="nav-link" href="{{ route('user.index') }}">{{ __('Users') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('meal.index') }}">{{ __('Meal') }}</a>
    </li>
@else
    <li class="nav-item">
        <a class="nav-link" href="{{ route('shop.index') }}">{{ __('Shop Meal') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('order.index') }}">{{ __('My Order') }}</a>
    </li>
@endif