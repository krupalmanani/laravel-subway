<?php

use Illuminate\Database\Seeder;

class SauceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sauce')->insertOrIgnore([
            ['id' => 1, 'sauce_title' => 'Sauce Choices 1'],
            ['id' => 2, 'sauce_title' => 'Sauce Choices 2'],
            ['id' => 3, 'sauce_title' => 'Sauce Choices 3'],
            ['id' => 4, 'sauce_title' => 'Sauce Choices 4'],
            ['id' => 5, 'sauce_title' => 'Sauce Choices 5'],
        ]);
    }
}
