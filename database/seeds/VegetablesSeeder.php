<?php

use Illuminate\Database\Seeder;

class VegetablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sandwich_vegetables')->insertOrIgnore([
            ['id' => 1, 'vegetables_title' => 'Vegetables Choices 1'],
            ['id' => 2, 'vegetables_title' => 'Vegetables Choices 2'],
            ['id' => 3, 'vegetables_title' => 'Vegetables Choices 3'],
        ]);
    }
}
