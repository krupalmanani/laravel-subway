<?php

use Illuminate\Database\Seeder;

class ExtraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('extra')->insertOrIgnore([
            ['id' => 1, 'extra_item' => 'extra bacon'],
            ['id' => 2, 'extra_item' => 'double meat'],
            ['id' => 3, 'extra_item' => 'extra cheese'],
        ]);
    }
}
