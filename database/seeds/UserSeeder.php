<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insertOrIgnore([
            ['id' => 1, 'name' => 'admin', 'email' => 'admin@gmail.com', 'password'=> bcrypt('admin@123'), 'role' => 'admin']
        ]);
    }
}
