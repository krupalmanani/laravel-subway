<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(BreadSeeder::class);
        $this->call(ExtraSeeder::class);
        $this->call(SauceSeeder::class);
        $this->call(SizeSeeder::class);
        $this->call(TasteSeeder::class);
        $this->call(VegetablesSeeder::class);
    }
}
