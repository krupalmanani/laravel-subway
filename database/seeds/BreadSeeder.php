<?php

use Illuminate\Database\Seeder;

class BreadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bread')->insertOrIgnore([
            ['id' => 1, 'bread_title' => 'Choices 1'],
            ['id' => 2, 'bread_title' => 'Choices 2'],
            ['id' => 3, 'bread_title' => 'Choices 3'],
            ['id' => 4, 'bread_title' => 'Choices 4'],
            ['id' => 5, 'bread_title' => 'Choices 5'],
        ]);
    }
}
