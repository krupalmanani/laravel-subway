<?php

use Illuminate\Database\Seeder;

class TasteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sandwich_taste')->insertOrIgnore([
            ['id' => 1, 'taste' => 'Taste Choices 1'],
            ['id' => 2, 'taste' => 'Taste Choices 2'],
            ['id' => 3, 'taste' => 'Taste Choices 3'],
        ]);
    }
}
