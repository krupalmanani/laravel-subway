<?php

use Illuminate\Database\Seeder;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bread_size')->insertOrIgnore([
            ['id' => 1, 'bread_size' => '15 cm'],
            ['id' => 2, 'bread_size' => '30 cm'],
        ]);
    }
}
