<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->uuid('meal_id');
            $table->unsignedBigInteger('bread')->nullable();
            $table->unsignedBigInteger('bread_size')->nullable();
            $table->string('oven_baked',1)->default('N');
            $table->unsignedBigInteger('sandwich_taste')->nullable();
            $table->unsignedBigInteger('extra')->nullable();
            $table->unsignedBigInteger('sandwich_vegetables')->nullable();
            $table->unsignedBigInteger('sauce')->nullable();
            $table->enum('status', ["open","close"])->default("open");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
