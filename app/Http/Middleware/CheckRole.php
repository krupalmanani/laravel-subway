<?php
namespace App\Http\Middleware;

use Auth;
use Closure;

class CheckRole
{

    public function handle($request, Closure $next,$role=''){

        $userRole = $request->user();

        if($userRole && $userRole->count() > 0)
        {
            $userRole = $userRole->role;
            $checkRole = 0;
            if($userRole == $role && $role =='admin'){
                $checkRole = 1;
            }elseif($userRole == $role && $role == 'user'){
                $checkRole = 1;
            }
            
            if($checkRole == 1)
                return $next($request);
            else
                return abort(401);
        }
        else
        {
            return redirect('login');
        }
    }
}