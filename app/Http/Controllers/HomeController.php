<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meal;
use App\OrderDetail;
use DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }
}
