<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meal;
use App\OrderDetail;
use DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ShopController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //$meal = Meal::whereDate('meal_date',date('Y-m-d'))->get()->toArray();
        $meal = Meal::all()->toArray();
        return view('shop.index', compact('meal'));
    }

    public function create($meal_id)
    {   
        $openMeal = Meal::where('is_open' ,"1")->pluck('id')->toArray();
        $user_id = Auth::user()->id;

        $prevOrder = OrderDetail::where('user_id',$user_id)->orderBy('created_at','DESC')->first();

        if(in_array($meal_id,$openMeal)){
            return view('shop.create', [
                'bread' => DB::table('bread')->get(),
                'bread_size' => DB::table('bread_size')->get(),
                'extra' => DB::table('extra')->get(),
                'sandwich_taste' => DB::table('sandwich_taste')->get(),
                'sandwich_vegetables' => DB::table('sandwich_vegetables')->get(),
                'sauce' => DB::table('sauce')->get(),
                'meal_id' => $meal_id,
                'prevOrder' => $prevOrder,
            ]);
        }else{
            abort(401);
        }

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'bread'    =>  'required',
            'bread_size'    =>  'required'
        ]);
        
        $user_id = Auth::user()->id;

        OrderDetail::insert([
            'user_id'    =>  $user_id,
            'meal_id'     =>  $request->get('meal_id'),
            'bread'     =>  $request->get('bread'),
            'bread_size'     =>  $request->get('bread_size'),
            'oven_baked'     =>  $request->get('oven_baked'),
            'sandwich_taste'     =>  $request->get('sandwich_taste'),
            'extra'     =>  is_array($request->get('extra')) ? implode(',',$request->get('extra')) : $request->get('extra'),
            'sandwich_vegetables'     =>  is_array($request->get('sandwich_vegetables')) ? implode(',',$request->get('sandwich_vegetables')) : $request->get('sandwich_vegetables') ,
            'sauce'     =>  is_array($request->get('sauce')) ? implode(',',$request->get('sauce')): $request->get('sauce'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
       
        return redirect()->route('shop.index')->with('success', 'Order successfully placed.');
    }
    
}
