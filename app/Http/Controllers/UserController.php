<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('user.index', compact('users'));
    }

    public function create()
    {
        return view('user.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'    =>  'required',
            'email'    =>  'required|unique:users,email',
            'password' => 'required| min:4|confirmed',
            'password_confirmation' => 'required| min:4'
        ]);

        User::insert([
            'name'     =>  $request->get('name'),
            'email'     =>  $request->get('email'),
            'role'     =>  (!empty($request->get('role'))) ? $request->get('role') : "user",
            'password'     =>  bcrypt($request->get('password')),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
       
        return redirect()->route('user.index')->with('success', 'User successfully added.');

    }

    public function edit($user_id){
        $user = User::find($user_id);
        return view('user.edit', compact('user'));
    }

    public function update(Request $request,$user_id){
        $this->validate($request, [
            'name'    =>  'required',
            'email'    =>  'required|unique:users,email,'.$user_id,
            'password' => 'required| min:4|confirmed',
            'password_confirmation' => 'required| min:4'
        ]);

        User::where('id', $user_id)
        ->update([
            'name'     =>  $request->get('name'),
            'email'     =>  $request->get('email'),
            'role'     =>  (!empty($request->get('role'))) ? $request->get('role') : "user",
            'password'     =>  bcrypt($request->get('password'))
        ]);
       
        return redirect()->route('user.index')->with('success', 'User successfully Updated.');
        
    }


}
