<?php

namespace App\Http\Controllers;

//use Request;
use Illuminate\Http\Request;
use Response;
use App\Meal;

class MealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meal = Meal::all()->toArray();
        return view('meal.index', compact('meal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('meal.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'meal_title'    =>  'required'
        ]);
        
        if(($request->get('is_open') == 'on')){
            Meal::where('is_open', "1")->update(array('is_open' => "0"));
        }

        $meal = new Meal([
            'meal_title'    =>  $request->get('meal_title'),
            'meal_date'     =>  $request->get('meal_date'),
            'is_open'     =>  ($request->get('is_open') == 'on') ? "1" : "0"
        ]);

        $meal->save();
        return redirect()->route('meal.index')->with('success', 'Meal Data Added');
    }

    public function status(Request $request)
    {
    
        $mealID = $request->get('mealId');
        $mealStatus = $request->get('status');

        Meal::where('is_open', "1")->update(array('is_open' => "0"));

        Meal::where('id', $mealID)->update(array('is_open' => $mealStatus));

        return response()->json(['result'=>'success']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meal = Meal::find($id);
        return view('meal.edit', compact('meal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $meal_id)
    {
        $this->validate($request, [
            'meal_title'    =>  'required'
        ]);
        
        if(($request->get('is_open') == 'on')){
            Meal::where('is_open', "1")->update(array('is_open' => "0"));
        }

        Meal::where('id',$meal_id)
        ->update([
            'meal_title'    =>  $request->get('meal_title'),
            'meal_date'     =>  $request->get('meal_date'),
            'is_open'     =>  ($request->get('is_open') == 'on') ? "1" : "0"
        ]);
       
        return redirect()->route('meal.index')->with('success', 'Meal Data Updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
