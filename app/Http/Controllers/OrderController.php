<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderDetail;
use Illuminate\Support\Facades\Auth;
use DB;

class OrderController extends Controller
{
    public function index($meal_id = 0)
    {
        $user_id = Auth::user()->id;
        $user_role = Auth::user()->role;
        $extras = DB::table('extra')->select('extra_item','id')->get()->keyBy('id')->toArray();
        $vegetables = DB::table('sandwich_vegetables')->select('vegetables_title','id')->get()->keyBy('id')->toArray();
        $sauce = DB::table('sauce')->select('sauce_title','id')->get()->keyBy('id')->toArray();
        
        $orders = OrderDetail::select('order_details.*', 'users.name','users.email', 'bread.bread_title', 'bread_size.bread_size', 'sandwich_taste.taste', 'meal.meal_title')
            ->join('meal', 'meal.id', '=', 'order_details.meal_id' )
            ->join('users', 'users.id', '=', 'order_details.user_id' )
            ->join('bread', 'bread.id', '=', 'order_details.bread' )
            ->join('bread_size', 'bread_size.id', '=', 'order_details.bread_size' )
            ->join('sandwich_taste', 'sandwich_taste.id', '=', 'order_details.sandwich_taste' )
            ->where('meal_id', $meal_id)
            ->get();
        return view('order.index', ['orders' => $orders, 'extras' => $extras, 'vegetables' => $vegetables, 'sauce' => $sauce]);
    }

    public function myOrder()
    {
        $user_id = Auth::user()->id;
        $extras = DB::table('extra')->select('extra_item','id')->get()->keyBy('id')->toArray();
        $vegetables = DB::table('sandwich_vegetables')->select('vegetables_title','id')->get()->keyBy('id')->toArray();
        $sauce = DB::table('sauce')->select('sauce_title','id')->get()->keyBy('id')->toArray();

        $orders = OrderDetail::select('order_details.*', 'users.name','users.email', 'bread.bread_title', 'bread_size.bread_size', 'sandwich_taste.taste', 'meal.meal_title')
            ->join('meal', 'meal.id', '=', 'order_details.meal_id' )
            ->join('users', 'users.id', '=', 'order_details.user_id' )
            ->join('bread', 'bread.id', '=', 'order_details.bread' )
            ->join('bread_size', 'bread_size.id', '=', 'order_details.bread_size' )
            ->join('sandwich_taste', 'sandwich_taste.id', '=', 'order_details.sandwich_taste' )
            ->where('user_id', $user_id)
            ->get();
        return view('order.myorder',  ['orders' => $orders, 'extras' => $extras, 'vegetables' => $vegetables, 'sauce' => $sauce]);
    }

    public function publicOrder($meal_id = 0)
    {
       
        $extras = DB::table('extra')->select('extra_item','id')->get()->keyBy('id')->toArray();
        $vegetables = DB::table('sandwich_vegetables')->select('vegetables_title','id')->get()->keyBy('id')->toArray();
        $sauce = DB::table('sauce')->select('sauce_title','id')->get()->keyBy('id')->toArray();

        $orders = OrderDetail::select('order_details.*', 'users.name','users.email', 'bread.bread_title', 'bread_size.bread_size', 'sandwich_taste.taste', 'meal.meal_title')
            ->join('meal', 'meal.id', '=', 'order_details.meal_id' )
            ->join('users', 'users.id', '=', 'order_details.user_id' )
            ->join('bread', 'bread.id', '=', 'order_details.bread' )
            ->join('bread_size', 'bread_size.id', '=', 'order_details.bread_size' )
            ->join('sandwich_taste', 'sandwich_taste.id', '=', 'order_details.sandwich_taste' )
            ->where('meal_id', $meal_id)
            ->get();
        return view('order.public',  ['orders' => $orders, 'extras' => $extras, 'vegetables' => $vegetables, 'sauce' => $sauce]);
    }

    public function status(Request $request)
    {
    
        $orderId = $request->get('orderId');
        $status = $request->get('status');

        OrderDetail::where('id', $orderId)->update(array('status' => $status));

        return response()->json(['result'=>'success']);

    }

    public function edit($id)
    {
        $order = OrderDetail::find($id);

        $user_id = Auth::user()->id;

        if(!empty($order) && $order->user_id == $user_id){
            return view('order.edit', [
                'order' => $order,
                'bread' => DB::table('bread')->get(),
                'bread_size' => DB::table('bread_size')->get(),
                'extra' => DB::table('extra')->get(),
                'sandwich_taste' => DB::table('sandwich_taste')->get(),
                'sandwich_vegetables' => DB::table('sandwich_vegetables')->get(),
                'sauce' => DB::table('sauce')->get()
            ]);
        }else{
            return abort(401);
        }
    }

    public function update(Request $request,$orderid){
        $this->validate($request, [
            'bread'    =>  'required',
            'bread_size'    =>  'required'
        ]);
        
        $user_id = Auth::user()->id;

        OrderDetail::where('id',$orderid)
        ->update([
            'user_id'    =>  $user_id,
            'meal_id'     =>  $request->get('meal_id'),
            'bread'     =>  $request->get('bread'),
            'bread_size'     =>  $request->get('bread_size'),
            'oven_baked'     =>  $request->get('oven_baked'),
            'sandwich_taste'     =>  $request->get('sandwich_taste'),
            'extra'     =>  is_array($request->get('extra')) ? implode(',',$request->get('extra')) : $request->get('extra'),
            'sandwich_vegetables'     =>  is_array($request->get('sandwich_vegetables')) ? implode(',',$request->get('sandwich_vegetables')) : $request->get('sandwich_vegetables') ,
            'sauce'     =>  is_array($request->get('sauce')) ? implode(',',$request->get('sauce')): $request->get('sauce'),
        ]);
       
        return redirect()->route('order.index')->with('success', 'Order successfully updated.');
    }
}
