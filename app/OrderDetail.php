<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_details'; 

    protected $fillable = [
        'user_id', 'meal_id', 'bread', 'bread_size', 'oven_baked', 'sandwich_taste', 'extra', 'sandwich_vegetables', 'sauce', 'status',
    ];
}
